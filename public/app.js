(function(){
    var App = angular.module('App', ['chart.js']);

    App.controller('clickOnName', function ($rootScope){
        $rootScope.setRecentSearch = function(value){
            $rootScope.recentSearch = value;
        };
    });

    App.controller('ListController', function($scope, $http) {
        $http.get('players/top?num=50')
            .then(function(res){
                $scope.data = res.data.sort(function (a, b){return b.rating - a.rating;});
            })
    });

    App.controller('HistoryController', function($scope, $http){
        $http.get('players/top?num=50&includeHistory=true')
            .then(function(res){
                $scope.data = res.data;
            })
    });

    App.controller('ChartController', function($scope, $http, $q, $filter){
        $http.get('players/top?num=6')
            .then(function(res){
                $scope.data = res.data.sort(function (a, b){return b.rating - a.rating;});
            })
            .then(function(){
                $scope.topNumPlayers = $scope.data.map(function(obj){
                    return obj.player;
                })
            })
            .then(function(){
                $scope.topPlayerSearch = [];
                var promises = [];
                for(var i = 0; i < $scope.topNumPlayers.length; i++){
                    var player = $scope.topNumPlayers[i];
                    promises.push(
                        $http.get('players/search?q=' + player + '&includeHistory=false') // write new search
                            .then(function(res){
                                $scope.topPlayerSearch.push(res.data[0].tourneyHistory); // write new search to exact match instead of this
                            })
                    )
                }
                $q.all(promises)
                    .then(function(){
                        $scope.histories = $scope.topPlayerSearch.map(function(obj){
                            return obj.map(function(obj){
                                return obj.rating;
                            })
                        })
                    })
                    .then(function(){
                        $scope.codes = $scope.topPlayerSearch[0].map(function(obj){
                            return obj.tourney;
                        })
                    })
                ;
            });
    });

    App.controller('TabController', function($scope){
        $scope.tab = 1;

        $scope.setTab = function(setTab){
            $scope.tab = setTab;
        };

        $scope.isSet = function(tabName){
            return $scope.tab === tabName;
        };
    });

    App.filter('NumberNoCommaFilter', function() {
        return function(value) {
            return parseInt(value, 10)
        }
    });

    App.filter('PlayerSearch', function() {
        return function(value) {
            return $scope.searchBox === value;
        }
    });

})();