
var https = require("https");
var AWS = require('aws-sdk');
AWS.config.region = 'us-west-2';
var host = "smash.gg";

var players = [];
var sets = [];

function parseGroup(tourney, phase, group, callback){
    var phasePath = "/api/-/resource/gg_api./tournament/"
        + tourney
        + "/event/melee-singles;expand=brackets;phaseId="
        + phase.id
        + ";phaseGroupId="
        + group.id;

    https.get({
        host: host,
        path: phasePath
    }, function (response) {
        var body = '';
        response.on('data', function (d) {
            body += d;
        });
        response.on('end', function () {

            var brackets = JSON.parse(body);
            var group = brackets.entities.selectedPhaseGroup[0];

            var seeds = brackets.entities.seeds;
            for (var i = 0; i < seeds.length; i++) {
                var seed = seeds[i];
                var entrantId = seed.entrantId
                for (playerMapId in seed.mutations.players) {
                    var player = seed.mutations.players[playerMapId];
                    var playerId = player.id;
                    var gamerTag = player.gamerTag;
                    players.push({
                        entrantId: entrantId,
                        playerId: playerId,
                        gamerTag: gamerTag
                    });
                }
            }

            for (var i = 0; i < brackets.entities.sets.length; i++) {
                var set = brackets.entities.sets[i];
                var setObj = {};
                setObj.entrant1Id = set.entrant1Id;
                setObj.entrant2Id = set.entrant2Id;
                setObj.entrant1Score = set.entrant1Score;
                setObj.entrant2Score = set.entrant2Score;
                sets.push(setObj);
            }
            callback();
        });
    });
}

function writeToS3(date, tourney){

    var s3Players = new AWS.S3({
        params: {
            Bucket: 'ssbratings',
            Key: 'parsedData/' + date + "_" + tourney + '/players'
        }
    });
    var s3Sets = new AWS.S3({
        params: {
            Bucket: 'ssbratings',
            Key: 'parsedData/' + date + "_" + tourney + '/sets'
        }
    });

    s3Players.upload({Body: JSON.stringify(players)}, function (err) {
        if (err) {
            console.log("error uploading players: ", err)
        } else {
            players = [];
        }
    });
    s3Sets.upload({Body: JSON.stringify(sets)}, function (err) {
        if (err) {
            console.log("error uploading players: ", err)
        } else {
            sets = [];
        }
    });
}

function parseSmashgg(tourney, code) {
    var path = "/api/-/resource/gg_api./tournament/" + tourney + "/event/melee-singles;;expand=[\"groups\",\"phase\"]";

    var date = Math.floor(Date.now() / 1000);

    var s3Code = new AWS.S3({params: {Bucket: 'ssbratings', Key: 'parsedData/' + date + "_" + tourney + '/code'}});
    s3Code.upload({Body: "{\"code\":\"" + code + "\"}"}, function (err) {
        if (err) {
            console.log("failure while uploading code: ", err);
        }
    });


    https.get({
        host: host,
        path: path
    }, function (response) {
        // Continuously update stream with data
        var body = '';
        response.on('data', function (d) {
            body += d;
        });
        response.on('end', function () {

            var parsed = JSON.parse(body);
            var phases = parsed.entities.phase;
            var nonAmateurPhases = phases.filter(function nonAmateur(phase) {
                return phase.name != "amateur" || phase.name != "round 1 pools" || phase.name != "round 1: pools";
            });

            var count = 0;
            var expected = 0;
            for (var p = 0; p < nonAmateurPhases.length; p++) {
                var phase = nonAmateurPhases[p];
                var phaseGroups = parsed.entities.groups.filter(function (group) {
                    return group.phaseId == phase.id;
                });

                for (var g = 0; g < phaseGroups.length; g++) {
                    var group = phaseGroups[g];
                    expected++;

                    parseGroup(tourney, phase, group, function(){
                        count++;
                        if(count === expected){
                            writeToS3(date, tourney);
                        }
                    });
                }
            }
        });
    });
}

module.exports = {
    parseSmashgg: parseSmashgg
};
