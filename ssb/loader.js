/**
 * Created by acaley on 7/30/16.
 */

var async = require('async');
var AWS = require('aws-sdk');
AWS.config.region  = 'us-west-2'
var glicko2 = require('glicko2');
var s3 = new AWS.S3();

var glickoSettings = {
    // tau : "Reasonable choices are between 0.3 and 1.2, though the system should
    //      be tested to decide which value results in greatest predictive accuracy."
    tau : 0.3,
    // rating : default rating
    rating : 1500,
    //rd : Default rating deviation
    //     small number = good confidence on the rating accuracy
    rd : 200,
    //vol : Default volatility (expected fluctation on the player rating)
    vol : 0.06
};
var ranking = new glicko2.Glicko2(glickoSettings);
var players = {};
var parsed = [];
var codes = [];
//list of players w/history & ELO

function findPlayer(code, entrantId){
    for(playerNum in players){
        var player = players[playerNum];

        if(player.tourneys[code] != undefined){
            if(player.tourneys[code].entrantId === entrantId) {
                return player;
            }
        }
    }
    return null;
}

function getCode(codeFile, callback){
    var code = new AWS.S3({
        params: {
            Bucket: 'ssbratings',
            Key: codeFile
        }
    });

    code.getObject(function(err, data){
        if(err){
            console.log("error downloading code file: " + err);
        } else {
            var codeObj = JSON.parse(data.Body);
            callback(err, codeObj.code);
        }
    });
}

function loadPlayerFile(code, path, callback){
/*
    var player = new AWS.S3({
        params: {
            Bucket: 'ssbratings',
            Key: path
        }
    });
*/
    var params = {
        Bucket: 'ssbratings',
        Key: path
    };

    s3.getObject(params, function(err, data){
        if(err){
            console.log("error downloading player file:" + err);
        } else {
            var playerData = JSON.parse(data.Body);
            for(var i = 0; i < playerData.length; i++){
                var player = playerData[i];
                if(players[player.playerId] == undefined) {
                    players[player.playerId] = {
                        id: player.playerId,
                        gamerTag: player.gamerTag,
                        tourneys: {},
                        matchHistory: [],
                        rating: ranking.makePlayer()
                    };
                }

                players[player.playerId].tourneys[code] = {
                    entrantId: player.entrantId
                };
            }
        }
        callback();
    });
}

function loadSetFile(code, matches, path, callback){

    var params = {
        Bucket: 'ssbratings',
        Key: path
    };

    s3.getObject(params, function(err, data){
        if(err){
            console.log("error downloading set file:" + err);
        } else {
            var setData = JSON.parse(data.Body);
            for(var i = 0; i < setData.length; i++) {
                var entrant1Id = setData[i].entrant1Id;
                var entrant1Score = setData[i].entrant1Score;
                var entrant2Id = setData[i].entrant2Id;
                var entrant2Score = setData[i].entrant2Score;

                if(entrant1Id === null || entrant2Id === null || entrant1Score === null || entrant2Score === null){
                    continue;
                }

                var player1 = findPlayer(code, entrant1Id);
                var player2 = findPlayer(code, entrant2Id);

                var result = 0;
                if(entrant1Score > entrant2Score){
                    result = 1;
                } if(entrant1Score === entrant2Score){
                    result = 0.5
                }

                matches.unshift([player1.rating, player2.rating, result]);

                player1.matchHistory.unshift({
                    opponent: player2.gamerTag,
                    playerScore: entrant1Score,
                    opponentScore: entrant2Score,
                    tournament: code
                });
                player2.matchHistory.unshift({
                    opponent: player1.gamerTag,
                    playerScore: entrant2Score,
                    opponentScore: entrant1Score,
                    tournament: code
                })
            }

        }
        callback();
    });
}

function loadAllPlayers(code, playerFiles, callback){
    var times = 0;
    for(var i = 0; i < playerFiles.length; i++){
        loadPlayerFile(code, playerFiles[i], function(){
            times++;
            if(times === playerFiles.length){
                callback(code);
            }
        });
    }
}

function loadAllSets(code, matches, setFiles, callback){
    async.eachSeries(setFiles, function(file, callback) {
            loadSetFile(code, matches, file, callback)
        },
        function(){
            callback();
        }
    );
/*
    var times = 0;
    for(var i = 0; i < setFiles.length; i++){
        loadSetFile(code, matches, setFiles[i], function(){
            times++;
            if(times === setFiles.length){
                callback();
            }
        });
    }
    */
}

function parseTourney(dir, callback){
    var tourneyData = new AWS.S3({
       params: {
           Bucket: 'ssbratings',
           Prefix: dir,
           Delimiter: '/'
       }
    });

    tourneyData.listObjects(function(err, data){
        if(err){
            console.log("error loading tourney data" + err);
        } else {
            var files = data.Contents.map(function(x){return x.Key});
            var codeFiles = files.filter(function(x){return x.indexOf("code") !== -1;});
            var playerFiles = files.filter(function(x) {return x.indexOf("players") !== -1;});
            var setFiles = files.filter(function(x){return x.indexOf("sets") !== -1;});

            //assuming 1 code file...
            var codeFile = codeFiles[0];
            getCode(codeFile, function(err,code){
                if(err){
                    console.log("error loading code " + err);
                } else {

                    loadAllPlayers(code, playerFiles, function(code){
                        var matches = []
                        loadAllSets(code, matches, setFiles, function(){
                            ranking.updateRatings(matches);
                            for(playerId in players){
                                var player = players[playerId];
                                if(player.tourneys[code] != null) {
                                    player.tourneys[code].rating = player.rating.getRating();
                                }
                            }
                            callback(code);
                        });
                    });


                }
            });

        }
    })
}

//loads from s3
function loadNewSmashData(callback) {
    var parsedData = new AWS.S3({
        params: {
            Bucket: 'ssbratings',
            Prefix: 'parsedData/',
            Delimiter: '/'
        }
    });
    parsedData.listObjects(function(err, data){
        if(err){
            console.log("error loading data: " + err);
        } else {

            async.eachSeries(data.CommonPrefixes, function(prefix, loopCallback){
                var dir = prefix.Prefix;
                if(!(dir in parsed)) {
                    parseTourney(dir, function (code) {
                        parsed.push(dir);
                        codes.push(code);
                        callback(code);
                        loopCallback();
                    })
                } else {
                    loopCallback();
                }
            });
        }
    });

}

//return one player
function getPlayer(name) {
    for(playerId in players){
        var player = players[playerId];
        if(player.gamerTag == name){
            return player;
        }
    }
    return null;
}

function getPlayers(search){
    var results = [];
    var lowerSearch = search.toLowerCase();
    for(playerId in players){
        var player = players[playerId];
        if(player.gamerTag.toLowerCase().indexOf(lowerSearch) != -1){
            results.push(player);
        }
    }

    return results;
}

function getTopPlayers(num){
    var playerArr = Object.keys(players);
    playerArr.sort(function(a,b) {
        return (players[b].rating.getRating() - players[a].rating.getRating());
    });
    var topPlayers = playerArr.slice(0, num);

    return topPlayers.map(function(playerId){
        return players[playerId];
    });
}

function getAllCodes(){
    return codes;
}

module.exports = {
    loadNewSmashData : loadNewSmashData,
    getPlayer: getPlayer,
    getPlayers: getPlayers,
    getTopPlayers: getTopPlayers,
    getAllCodes: getAllCodes
};
/*

loadNewSmashData(function(code){
    var playerArr = Object.keys(players);
    playerArr.sort(function(a,b) {
        return (players[b].rating.getRating() - players[a].rating.getRating());
    });


    console.log(code);
    console.log(players[playerArr[0]].gamerTag + ":" + players[playerArr[0]].rating.getRating());
    console.log(players[playerArr[1]].gamerTag + ":" + players[playerArr[1]].rating.getRating());
    console.log(players[playerArr[2]].gamerTag + ":" + players[playerArr[2]].rating.getRating());
    console.log(players[playerArr[3]].gamerTag + ":" + players[playerArr[3]].rating.getRating());
    console.log(players[playerArr[4]].gamerTag + ":" + players[playerArr[4]].rating.getRating());
    console.log(players[playerArr[5]].gamerTag + ":" + players[playerArr[5]].rating.getRating());
    console.log(players[playerArr[6]].gamerTag + ":" + players[playerArr[6]].rating.getRating());
    console.log(players[playerArr[7]].gamerTag + ":" + players[playerArr[7]].rating.getRating());
    console.log(players[playerArr[8]].gamerTag + ":" + players[playerArr[8]].rating.getRating());
    console.log(players[playerArr[9]].gamerTag + ":" + players[playerArr[9]].rating.getRating());
    console.log(players[playerArr[10]].gamerTag + ":" + players[playerArr[10]].rating.getRating());

});

*/