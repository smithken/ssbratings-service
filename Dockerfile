FROM readytalk/nodejs

ENV docker-updated TODAY_DATE

ENV AWS_ACCESS_KEY_ID ACCESS_KEY

ENV AWS_SECRET_ACCESS_KEY SECRET_KEY

WORKDIR /
RUN git clone https://gitlab.com/smithken/ssbratings-service.git app

WORKDIR /app
RUN npm install

CMD []
ENTRYPOINT ["/nodejs/bin/npm", "start"]
EXPOSE 3000