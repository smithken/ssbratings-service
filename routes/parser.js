var express = require('express');
var router = express.Router();

var parser = require('../ssb/parser');

router.get('/', function(req, res, next) {
    parser.parseSmashgg(req.query.tourney, req.query.code);
    res.send('parsing: ' + req.query.tourney + ' short code: ' + req.query.code);
});

module.exports = router;
