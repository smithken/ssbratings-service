/**
 * Created by acaley on 7/30/16.
 */

var express = require('express');
var router = express.Router();
var loader = require('../ssb/loader');
var schedule = require('node-schedule');

loader.loadNewSmashData(function(code){
    if(code) {
        console.log("loaded new tourney: " + code);
    }
});
/*
 var job = schedule.scheduleJob('* /5 * * *', function(){
 loader.loadNewSmashData(function(code){
 if(code) {
 console.log("loaded new tourney: " + code);
 }
 });
 });
 */

//player/search/{name}
router.get('/search', function(req,resp, next){
    var players = loader.getPlayers(req.query.q);

    var output = players.map(function(player){
        var tourneys = [];
        var codes = loader.getAllCodes();
        var lastTourneyRating = 1500;
        for(var i = 0 ; i < codes.length; i++){
            var code = codes[i];

            if(player.tourneys[code] != null){
                lastTourneyRating = player.tourneys[code].rating;
            }

            tourneys.push({
               rating: lastTourneyRating,
               tourney: code
            });
        }

        if(req.query.includeHistory === "false"){
            return{
                player: player.gamerTag,
                rating: player.rating.getRating(),
                tourneyHistory: tourneys
            };
        }
        else{
            return {
                player: player.gamerTag,
                rating: player.rating.getRating(),
                matchHistory: player.matchHistory,
                tourneyHistory: tourneys
            };
        }
    });

    resp.send(JSON.stringify(output));

});

router.get('/top', function(req,resp){
    var topPlayers = loader.getTopPlayers(req.query.num);

    var top = topPlayers.map(function(player){
        if(req.query.includeHistory === "true"){
            return{
                player: player.gamerTag,
                history: player.matchHistory
            }
        }
        else{
            return {
                player: player.gamerTag,
                rating: player.rating.getRating()
            };
        }
    });
    resp.send(JSON.stringify(top));
});

module.exports = router;